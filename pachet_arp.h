#ifndef PACHET_ARP_H
#define PACHET_ARP_H
#include <cstdio>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cstdio>
#include <cstring>
#include <future>
#include <chrono>
#include <thread>


#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/ethernet.h>
#include <net/if.h>
#include <linux/if_packet.h>
#include <sys/time.h>

class Arp {
private:

    const uint16_t HTYPE; //1
    const uint16_t PTYPE; //0x0800
    const uint8_t HLEN; //6
    const uint8_t PLEN; //4
    const uint16_t OPER; //2 -> Reply
    const uint16_t ET_ARP; // 0x0806
    const uint16_t ARP_REPLY_LENGTH; //60

    struct HEADER {
        uint16_t HTYPE; //Hardware Type
        uint16_t PTYPE; //Protocol Type
        uint8_t HLEN; //Hardware Address Length
        uint8_t PLEN; //Protocol Address Length
        uint16_t OPER; //Operation
        uint8_t SHA[6]; //Sender Hardware Address
        uint8_t SPA[4]; //Sender Protocol Address
        uint8_t THA[6]; //Target Hardware Address
        uint8_t TPA[4]; //Target Protocol Address

    } arp_header;

    struct ifreq interfata;
    struct sockaddr_ll saLinkLevel;
    char numeInterfata[IFNAMSIZ];

    int getMac();
    void printHeader(struct HEADER head);
    int requestMac(const char* ip, const char *ipSRC = NULL);
    uint8_t * receiveMac(const char *IP);

    uint8_t *pachet;

    char senderIP[16];
    char targetIP[16];
    char spoofIP[16];


public:

    Arp();
    virtual ~Arp();
    void Send();
    void Initializare();
    void Receive();
    void setNumeInterfata(const char numeInterfata[IFNAMSIZ]);
    void setTargetIP(const char ip[16]);
    void setSenderIP(const char ip[16]);
    void setSpoofIP(const char ip[16]);
};



#endif /* PACHET_ARP_H */

