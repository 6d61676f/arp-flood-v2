#ifndef INTERFETE_H
#define INTERFETE_H
#include <ifaddrs.h>
#include <iostream>
#include <cstdlib>
#include <string>
#include <map>

class Interfete {
private:
    struct ifaddrs * curent, *urmator;
    std::map<std::string, std::string > nume_interfete;
    std::map<std::string, std::string > tabel_arp;
    int eroare_interfete;
    int getInterfete();
    int getArp();
    const char *arp_cache;
public:
    const std::map<std::string, std::string > getInterfete()const;
    void printInterfete();
    void printArp();
    Interfete();
    ~Interfete();
};



#endif /* INTERFETE_H */

