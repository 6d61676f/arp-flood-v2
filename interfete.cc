#include "interfete.h"
#include "pachet_arp.h"
#include <sys/types.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fstream>

Interfete::Interfete() {
    arp_cache = "/proc/net/arp";
    eroare_interfete = getifaddrs(&curent);
    std::cout << ((eroare_interfete == 0) ? "Am apelat getifaddrs" : "Eroare la getifaddrs") << std::endl;
    getInterfete();
    getArp();
}

Interfete::~Interfete() {
    freeifaddrs(curent);
}

int Interfete::getArp() {
    uint8_t linie[0x1000];
    uint8_t ip[16];
    uint8_t hwaddr[18];
    std::ifstream in(arp_cache);

    int err = in.is_open();

    std::cerr << ((in.is_open()) ? "Am deschis /proc/net/arp" : "Eroare la /proc/net/arp") << std::endl;

    in.getline((char*) linie, 0x1000);
    while (!in.eof()) {
        in >> ip >> linie >> linie >> hwaddr >> linie>>linie;
        tabel_arp.insert(std::pair<std::string, std::string >((char*) &ip, (char*) & hwaddr));
    }

    return err;
}

int Interfete::getInterfete() {
    if (eroare_interfete != 0) {
        std::cerr << "Nu avem interfete! Eroare";
        return EXIT_FAILURE;
    } else {
        struct sockaddr_in *adresa;
        for (urmator = curent; urmator != NULL; urmator = urmator->ifa_next) {
            if (urmator->ifa_addr->sa_family == AF_INET) {
                adresa = (struct sockaddr_in *) urmator->ifa_addr;
                nume_interfete.insert(std::pair<std::string, std::string>(urmator->ifa_name, inet_ntoa(adresa->sin_addr)));
            }
        }
        return EXIT_SUCCESS;
    }
}

const std::map<std::string, std::string > Interfete::getInterfete()const {
    return nume_interfete;
}

void Interfete::printArp() {

    std::map<std::string, std::string>::iterator it;
    for (it = tabel_arp.begin(); it != tabel_arp.end(); ++it) {
        std::cout << "IP:" << it->first << " => MAC " << it->second << std::endl;
    }

}

void Interfete::printInterfete() {

    std::map<std::string, std::string >::iterator it;
    for (it = nume_interfete.begin(); it != nume_interfete.end(); ++it) {
        std::cout << "Interfata:" << it->first << " => Adresa:" << it->second << std::endl;
    }

}
